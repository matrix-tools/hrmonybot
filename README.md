# hrmony
A [maubot](https://github.com/maubot/maubot) to upload your receipts to [Hrmony](https://hrmony.de) via email.

[Hrmony](https://hrmony.de) is a service for companys which like to provide food benefits to their employees.
(At least for german companies)

Because of some governmental restrictions you have to upload your food receipts more or less daily to hrmony.
At the moment they provide a whatsapp chat to send your receipts or sending it by email.

This bot supports you, by sending the receipt as email from an elected matrix room.
You just have to post a picture from the receipt into this matrix room.

## Requirements

You must have an email registered at hrmony.de where you can post receipts to.

Next you can install the hrmony bot.

## Installation

1. Modify the bot config

````
encryption_key: '' 
maubot_user: ''
hrmony_recipient: ''
bcc_recepient: ''
````

- encryption_key - your smtp server credentials are stored encrypted with this encryptionkey in the maubot database
- maubot_user - the user under which maubot ist runnig
- hrmony_recipient - is the email address from hrmony.de where to send the emails to
- bcc_recepient - another (optional) mail address for a bcc

## Usage

1. Invite maubot

create a room and invite the maubot user

2. Set the room as hrmony room

You need to set a room as hrmony room because all pictures posted in this room are sent by email without providing a specific maubot command

Just post 
````
!hrmony set
````

3. provide some smtp credentials

````
!hrmony credentials your@email.example smtp_user smtp_password smtp.server.example 587
````

4. Post your receipts in this room
