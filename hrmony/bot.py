# hrmony - A maubot plugin to upload your receipts to hrmony via email.
# Copyright (C) 2021 uaf
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.

from datetime import datetime
from typing import Type
from mautrix.util.config import BaseProxyConfig
from maubot import Plugin, MessageEvent
from maubot.handlers import command
from mautrix.types import (EventType, StateEvent, MediaMessageEventContent)
from maubot.handlers import event
from .util import Config, SmtpCredentials
from .mail import send_mail
from .db import HrmonyDatabase


class HrmonyBot(Plugin):
    db: HrmonyDatabase
    encryption_key: str
    maubot_user: str
    hrmony_recipient: str
    bcc_recepient: str

    @classmethod
    def get_config_class(cls) -> Type[BaseProxyConfig]:
        return Config

    @staticmethod
    def _str_is_empty(value: str) -> bool:
        if not value:
            return True

        if not value.strip():
            return True

        return False

    def _is_hrmony_room(self, evt: StateEvent) -> bool:
        if HrmonyBot._str_is_empty(self.maubot_user):
            return False

        if evt.sender == self.maubot_user:
            return False

        hrmony_room_id = self.db.get_hrmony_room(evt.sender)
        if evt.room_id != hrmony_room_id:
            return False

        return True

    async def start(self) -> None:
        self.on_external_config_update()
        self.db = HrmonyDatabase(self.database, self.encryption_key)

    def on_external_config_update(self) -> None:
        self.config.load_and_update()

        self.encryption_key = self.config["encryption_key"]
        self.maubot_user = self.config["maubot_user"]
        self.hrmony_recipient = self.config["hrmony_recipient"]
        self.bcc_recepient = self.config["bcc_recepient"]

    @command.new(name="hrmony")
    async def hrmony(self, evt: MessageEvent) -> None:
        await evt.reply("Hello!")

    @hrmony.subcommand("hello", help="just say hello")
    async def hello_world(self, evt: MessageEvent) -> None:
        await evt.reply("Hello, World!")

    @hrmony.subcommand("credentials", help="set smtp server credentials", aliases=("setup",))
    @command.argument("email", required=True)
    @command.argument("user", required=True)
    @command.argument("password", required=True)
    @command.argument("server", required=True)
    @command.argument("port", required=True)
    async def set_credentials(self, evt: MessageEvent, email: str, user: str, password: str, server: str, port: str) -> None:

        if not self._is_hrmony_room(evt):
            return

        if HrmonyBot._str_is_empty(email) or HrmonyBot._str_is_empty(email) or HrmonyBot._str_is_empty(user) or HrmonyBot._str_is_empty(password) or HrmonyBot._str_is_empty(server) or HrmonyBot._str_is_empty(port):
            await evt.reply('set smtp server credentials (email user password server port)')

        credentials = SmtpCredentials(evt.sender, datetime.utcnow(), email, user, password, server, int(port))
        self.db.insert_smtp_credentials(credentials)
        await evt.reply('sucessfully set email credentials')

    @hrmony.subcommand("deletecredentials", help="delete smtp credentials")
    async def delete_credentials(self, evt: MessageEvent) -> None:
        self.db.delete_smtp_credentials(evt.sender)
        await evt.reply("Smtp credentials successfully deleted")

    @hrmony.subcommand("set", help="send posted pictures in this room to hrmony")
    async def register_room(self, evt: MessageEvent) -> None:
        self.db.set_hrmony_room(evt.sender, evt.room_id)
        await evt.reply("Set this room as hrmony room!")

    @hrmony.subcommand("check", help="try to load the smtp credentials in order to check if there might be an error with the encryption key (e.g. encryption key changed)")
    async def check_credentials(self, evt: MessageEvent) -> None:
        try:
            credentials = self.db.get_smtp_credentials(evt.sender)
            if credentials:
                await evt.reply("Successfully loaded credentials")
            else:
                await evt.reply("Cannot load credentials")
        except Exception as e:
            await evt.reply(e)

    @hrmony.subcommand("unset", help="do not send pictures from this room to hrmony and delete credentials")
    async def unregister_room(self, evt: MessageEvent) -> None:
        self.db.delete_hrmony_room(evt.sender)
        self.db.delete_smtp_credentials(evt.sender)
        await evt.reply("This room is no hrmony room anymore :(")

    @event.on(EventType.ROOM_MESSAGE)
    async def handle_message(self, evt: StateEvent) -> None:
        if not self._is_hrmony_room(evt):
            return

        if HrmonyBot._str_is_empty(self.hrmony_recipient):
            await evt.reply("please provide a hrmony_recipient config entry")
            return

        if isinstance(evt.content, MediaMessageEventContent):
            credentials = self.db.get_smtp_credentials(evt.sender)
            if credentials is None:
                await evt.reply("please provide a smpt server configuration")
                return

            data = await self.client.download_media(evt.content.url)

            try:
                bcc = None if HrmonyBot._str_is_empty(self.bcc_recepient) else self.bcc_recepient
                send_mail(credentials, self.hrmony_recipient, data, evt.content.info["mimetype"], bcc)
                await evt.reply('uploading your receipt was sucessful')
            except Exception as ex:
                self.log.error(ex)
                await evt.reply(evt.room_id, 'Ups')
                await evt.reply(evt.room_id, ex)
