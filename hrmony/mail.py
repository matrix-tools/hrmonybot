# hrmony - A maubot plugin to upload your receipts to hrmony via email.
# Copyright (C) 2021 uaf
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.

import smtplib
import ssl
from email import encoders
from email.mime.base import MIMEBase
from email.mime.multipart import MIMEMultipart
from email.mime.text import MIMEText
from .util import SmtpCredentials


class UnkownMimetypeException(Exception):

    def __init__(self, *args: object) -> None:
        super().__init__(*args)


def get_filename_from_mimetype(mime_type: str) -> str:
    if mime_type == "image/png":
        return "receipt.png"
    if mime_type == "image/jpg":
        return "receipt.jpg"
    if mime_type == "image/jpeg":
        return "receipt.jpeg"

    raise UnkownMimetypeException(mime_type)


def send_mail(credentials: SmtpCredentials, receiver: str, data: bytes, mimetype: str, bcc: str = None):
    subject = "Receipt"
    body = ""
    password = credentials.password

    # Create a multipart message and set headers
    message = MIMEMultipart()
    message["From"] = credentials.email
    message["To"] = receiver
    message["Subject"] = subject

    recipients = [receiver]
    if bcc is not None:
        message["Bcc"] = bcc
        recipients.append(bcc)

    # Add body to email
    message.attach(MIMEText(body, "plain"))

    part = MIMEBase("application", "octet-stream")
    part.set_payload(data)

    # Encode file in ASCII characters to send by email
    encoders.encode_base64(part)

    # Add header as key/value pair to attachment part
    part.add_header(
        "Content-Disposition",
        f"attachment; filename={get_filename_from_mimetype(mimetype)}",
    )

    # Add attachment to message and convert message to string
    message.attach(part)
    text = message.as_string()

    # Log in to server using secure context and send email
    context = ssl.create_default_context()

    with smtplib.SMTP(credentials.smtp_server, credentials.smtp_port) as server:
        # Secure the connection
        server.starttls(context=context)
        server.login(credentials.username, password)
        server.sendmail(credentials.email, recipients, text)
