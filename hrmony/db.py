# hrmony - A maubot plugin to upload your receipts to hrmony via email.
# Copyright (C) 2021 uaf
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.

from sqlalchemy.sql.sqltypes import String
from typing import Optional
from sqlalchemy_utils import EncryptedType
from sqlalchemy_utils.types.encrypted.encrypted_type import AesEngine
from sqlalchemy import (Column, Unicode, Integer, DateTime, Table, MetaData,
                        select)
from sqlalchemy.engine.base import Engine

from .util import SmtpCredentials
import pytz


class HrmonyDatabase:
    smtp_credentials: Table
    hrmony_rooms: Table
    db: Engine

    def __init__(self, db: Engine, secret_key: str) -> None:
        self.db = db

        meta = MetaData()
        meta.bind = db

        self.smtp_credentials = Table("smtp_credentials",
                                      meta,
                                      Column("user_id", String(255), primary_key=True),
                                      Column("date", DateTime, nullable=False),
                                      Column("email", EncryptedType(Unicode, secret_key, AesEngine, 'pkcs5')),
                                      Column("username", EncryptedType(Unicode, secret_key, AesEngine, 'pkcs5')),
                                      Column("password", EncryptedType(Unicode, secret_key, AesEngine, 'pkcs5')),
                                      Column("smtp_server", EncryptedType(Unicode, secret_key, AesEngine, 'pkcs5')),
                                      Column("smtp_port", Integer, nullable=False)
                                      )

        self.hrmony_rooms = Table("hrmony_rooms", meta,
                                  Column("user_id", String(255), primary_key=True),
                                  Column("room_id", String, nullable=False),
                                  )

        meta.create_all()

    def insert_smtp_credentials(self, credentials: SmtpCredentials) -> None:
        with self.db.begin() as tx:
            tx.execute(self.smtp_credentials.delete())

            tx.execute(self.smtp_credentials.insert()
                       .values(user_id=credentials.user_id, date=credentials.date, email=credentials.email,
                       username=credentials.username, password=credentials.password,
                       smtp_server=credentials.smtp_server, smtp_port=credentials.smtp_port))

    def delete_smtp_credentials(self, user_id: str) -> None:
        with self.db.begin() as tx:
            tx.execute(self.smtp_credentials.delete().where(self.smtp_credentials.c.user_id == user_id))

    def get_smtp_credentials(self, user_id: str) -> Optional[SmtpCredentials]:
        first_row = self.db.execute(select([self.smtp_credentials]).where(self.smtp_credentials.c.user_id == user_id)).first()
        if first_row is not None:
            info = SmtpCredentials(user_id=first_row[0], date=first_row[1].replace(tzinfo=pytz.UTC),
                                   email=first_row[2], username=first_row[3], password=first_row[4],
                                   smtp_server=first_row[5], smtp_port=first_row[6])
            return info
        return None

    def set_hrmony_room(self, user_id: str, room_id: str) -> None:
        with self.db.begin() as tx:
            tx.execute(self.hrmony_rooms.delete().where(self.hrmony_rooms.c.user_id == user_id))
            tx.execute(self.hrmony_rooms.insert().values(user_id=user_id, room_id=room_id))

    def delete_hrmony_room(self, user_id: str) -> None:
        with self.db.begin() as tx:
            tx.execute(self.hrmony_rooms.delete().where(self.hrmony_rooms.c.user_id == user_id))

    def get_hrmony_room(self, user_id: str) -> str:
        first_row = self.db.execute(select([self.hrmony_rooms]).where(self.hrmony_rooms.c.user_id == user_id)).first()
        if first_row is not None:
            return first_row[1]
        return None
