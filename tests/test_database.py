import unittest
from sqlalchemy import create_engine
from datetime import datetime
from hrmony import db
from hrmony.util import SmtpCredentials
import pytz
from sqlalchemy_utils.types.encrypted.padding import InvalidPaddingError


class DatabaseTests(unittest.TestCase):

    def test_that_insert_and_get_credentials_works_as_expected(self):
        engine = create_engine('sqlite:///:memory:', echo=True)
        sut = db.HrmonyDatabase(engine, "MySecretKey")        
        
        smtp_credentials = SmtpCredentials("my_user_id")
        smtp_credentials.date = datetime.now(tz=pytz.UTC)
        smtp_credentials.email = "my@mail.de"
        smtp_credentials.username = "myusername"
        smtp_credentials.password = "mypassword"
        smtp_credentials.smtp_server = "mysmtpserver"
        smtp_credentials.smtp_port = 587

        sut.insert_smtp_credentials(smtp_credentials)

        result = sut.get_smtp_credentials("my_user_id")

        self.assertIsNotNone(result)
        self.assertEqual(result, smtp_credentials)


    def test_that_credentials_are_not_loaded_with_different_user_id(self):
        engine = create_engine('sqlite:///:memory:', echo=True)
        sut = db.HrmonyDatabase(engine, "MySecretKey")        
        
        smtp_credentials = SmtpCredentials("my_user_id")
        smtp_credentials.date = datetime.now(tz=pytz.UTC)
        smtp_credentials.email = "my@mail.de"
        smtp_credentials.username = "myusername"
        smtp_credentials.password = "mypassword"
        smtp_credentials.smtp_server = "mysmtpserver"
        smtp_credentials.smtp_port = 587

        sut.insert_smtp_credentials(smtp_credentials)

        result = sut.get_smtp_credentials("other_user_id")

        self.assertIsNone(result)

    def test_that_credentials_are_not_correctly_encrypted_when_secret_is_changed(self):
        engine = create_engine('sqlite:///:memory:', echo=True)
        sut = db.HrmonyDatabase(engine, "MySecretKey")        
        
        smtp_credentials = SmtpCredentials("my_user_id")
        smtp_credentials.date = datetime.now(tz=pytz.UTC)
        smtp_credentials.email = "my@mail.de"
        smtp_credentials.username = "myusername"
        smtp_credentials.password = "mypassword"
        smtp_credentials.smtp_server = "mysmtpserver"
        smtp_credentials.smtp_port = 587
        sut.insert_smtp_credentials(smtp_credentials)

        get_db = db.HrmonyDatabase(engine, "AnotherKey") 
        with self.assertRaises(InvalidPaddingError):
            get_db.get_smtp_credentials("my_user_id")

    def test_that_result_is_none_when_no_credentials_are_inserted(self):
        engine = create_engine('sqlite:///:memory:', echo=True)
        sut = db.HrmonyDatabase(engine, "MySecretKey")        

        result = sut.get_smtp_credentials("my_user_id")
        self.assertIsNone(result)

    def test_that_credentials_are_none_when_credentials_are_deleted(self):
        engine = create_engine('sqlite:///:memory:', echo=True)
        sut = db.HrmonyDatabase(engine, "MySecretKey")        
        
        smtp_credentials = SmtpCredentials("my_user_id")
        smtp_credentials.date = datetime.now(tz=pytz.UTC)
        smtp_credentials.email = "my@mail.de"
        smtp_credentials.username = "myusername"
        smtp_credentials.password = "mypassword"
        smtp_credentials.smtp_server = "mysmtpserver"
        smtp_credentials.smtp_port = 587
        sut.insert_smtp_credentials(smtp_credentials)

        result = sut.get_smtp_credentials("my_user_id")
        self.assertIsNotNone(result)

        sut.delete_smtp_credentials("my_user_id")
        result = sut.get_smtp_credentials("my_user_id")
        self.assertIsNone(result)

    def test_that_insert_and_get_room_id_works_as_expected(self):
        engine = create_engine('sqlite:///:memory:', echo=True)
        sut = db.HrmonyDatabase(engine, "MySecretKey")        

        sut.set_hrmony_room("my_user_id", "MyRoomId")
        result = sut.get_hrmony_room("my_user_id")
        self.assertEqual(result, "MyRoomId")

    def test_that_hrmony_room_is_deleted_correctly(self):
        engine = create_engine('sqlite:///:memory:', echo=True)
        sut = db.HrmonyDatabase(engine, "MySecretKey")        

        sut.set_hrmony_room("my_user_id", "MyRoomId")
        result = sut.get_hrmony_room("my_user_id")
        self.assertEqual(result, "MyRoomId")

        sut.delete_hrmony_room("my_user_id")

        result = sut.get_hrmony_room("my_user_id")
        self.assertIsNone(result)
        