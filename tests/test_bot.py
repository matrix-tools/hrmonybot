import unittest
from unittest.mock import MagicMock
from sqlalchemy import create_engine
from hrmony.bot import HrmonyBot


class DatabaseTests(unittest.TestCase):
    
    def test_str_is_empty(self):
        engine = create_engine('sqlite:///:memory:', echo=True)
        sut = HrmonyBot(MagicMock(), MagicMock(), MagicMock(), 'myinstance', MagicMock(), None, engine, None, "", None)
        
        result = sut._str_is_empty("")
        self.assertTrue(result)

        result = sut._str_is_empty("  ")
        self.assertTrue(result)

        result = sut._str_is_empty(None)
        self.assertTrue(result)

        result = sut._str_is_empty("MyStr")
        self.assertFalse(result)
        